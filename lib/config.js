const path = require('path');
const argv = require('yargs').argv;

class Config {
  constructor() {
    this.data = {
      logLevel: argv.logLevel || 'info',
      fileLogLevel: argv.fileLogLevel || 'debug',
      logDir: (argv.seleniumProxyLogDir || process.env['GITLAB_SELENIUM_PROXY_LOG_DIR']) ? path.resolve(__dirname, '../', (argv.gitlabSeleniumProxyLogDir || process.env['GITLAB_SELENIUM_PROXY_LOG_DIR'])) : path.resolve(__dirname, '../logs'),
      targetEndpoint: argv.targetSeleniumRemoteUrl || process.env['GITLAB_TARGET_SELENIUM_REMOTE_URL'] || 'http://localhost:4444/wd/hub'
    };
  }

  set(key, value) {
    this.data[key] = value;
  }

  get(key) {
    return this.data[key];
  }
}

module.exports = new Config();
