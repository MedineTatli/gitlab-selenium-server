const Promise = require('bluebird');
const url = require('url');
const urlJoin = require('url-join');
const request = Promise.promisify(require('request'));

//const { logger } = require('./log');

const logMap = {
  /* * /
  :sessionId: {
    browser: [],
    client: [],
    driver: [],
    performance: [],
    server: [],
    // GitLab proprietary log, used for CI views
    gitlab: []
  }
  /* */
};

function updateLog(sessionId, type, logValue) {
  logMap[sessionId] = (logMap[sessionId] || {});
  const resultantLog = (logMap[sessionId][type] || []).concat(logValue);
  logMap[sessionId][type] = resultantLog;
  //logger.log('debug', 'updateLog resultantLog', sessionId, resultantLog);
  return resultantLog;
}


function fetchLogTypes(target, sessionId) {
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedTargetUrl, {
    pathname: urlJoin('/', parsedTargetUrl.pathname, `/session/${sessionId}/log/types`),
  }));

  return request({
    method: 'get',
    uri: resultantTargetUrl,
    json: true
  })
    .then((res) => {
      if(res.status === 200) {
        return res.body.value;
      }
    });
}

function fetchLog(target, sessionId, type) {
  const parsedTargetUrl = url.parse(target);

  const resultantTargetUrl = url.format(Object.assign({}, parsedTargetUrl, {
    pathname: urlJoin('/', parsedTargetUrl.pathname, `/session/${sessionId}/log`),
  }));

  return request({
    method: 'post',
    uri: resultantTargetUrl,
    // For some reason the `json` option results in an error
    // `java.lang.ClassCastException` -> `java.lang.String cannot be cast to java.util.HashMap`
    //json: true,
    body: JSON.stringify({
      type
    })
  })
    .then((res) => {
      let log = [];
      try {
        const resData = JSON.parse(res.body);
        log = resData.value;
      } catch(err) {
        // swallow
      }

      return log;
    });
}

function fetchAllLogs(target, sessionId) {
  return fetchLogTypes(target, sessionId)
    .then((types = []) => {
      const logPromises = types.map((type) => {
        return fetchLog(target, sessionId, type)
          .then((log) => {
            return updateLog(sessionId, type, log);
          });
      });

      return Promise.all(logPromises)
        .then(() => logMap[sessionId]);
    });
}

module.exports = {
  updateLog,
  fetchLogTypes,
  fetchLog,
  fetchAllLogs
};
