const {
  isOpeningUrl,
  isClicking,
  isSendingKeys,
  isExecutingJavascript
} = require('./identify-command-from-req');

const screenshotTriggers = {
  isOpeningUrl,
  isClicking,
  isSendingKeys,
  isExecutingJavascript
};

/**
 * Retuns true if any request contains a command that triggers a screenshot
 * @param {*} req
 */
function doesRequestTriggerScreenshot(req) {
  return Object.keys(screenshotTriggers).some((trigger) => screenshotTriggers[trigger](req).result);
}

module.exports = doesRequestTriggerScreenshot;
