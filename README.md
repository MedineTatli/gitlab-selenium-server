# GitLab Selenium Proxy/Server

This project is a work in progress (WIP).

## General Usage

Say you had your own Selenium server running on port `4444`
(see the "Start your own Selenium sever" below), you can point the GitLab Selenium
server proxy at that local server with the `GITLAB_TARGET_SELENIUM_REMOTE_URL` URL.

```sh
# macOS/Linux
GITLAB_TARGET_SELENIUM_REMOTE_URL=http://localhost:4444/wd/hub npm start

# Windows
set GITLAB_TARGET_SELENIUM_REMOTE_URL="http://localhost:4444/wd/hub"&&npm start
```

Now you point your Selenium tests at the GitLab Selenium server, `http://localhost:4545/wd/hub`


## GitLab CI Usage

#### Run the Selenium proxy and server directly in the main app container

`.gitlab-ci.yml` (untested)
```yml
image: node:boron

test:
  stage: test
  script:
    # Dependencies for chromedriver, https://gist.github.com/mikesmullin/2636776#gistcomment-1742414
    # Otherwise we get this error: "error while loading shared libraries: libnss3.so: cannot open shared object file: No such file or directory"
    - apt-get update -q -y
    - apt-get --yes install libnss3
    - apt-get --yes install libgconf-2-4

    # Install chrome
    # Based off of
    # - https://gitlab.com/gitlab-org/gitlab-build-images/blob/9dadb28021f15913a49897126a0cd6ab0149e44f/scripts/install-chrome
    # - https://askubuntu.com/a/510186/196148
    #
    # Add key
    - curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
    # Add repo
    - echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list
    - apt-get update -q -y
    - apt-get install -y google-chrome-stable

    - npm install chromedriver -g
    - npm install https://gitlab.com/gitlab-org/gitlab-selenium-server.git -g
    # The `&` at the end causes it to run in the background and not block the following commands
    - nohup chromedriver --port=4444 --url-base=wd/hub &
    - nohup gitlab-selenium-server &

    # Run your tests
    - npm test

    # Show the logs for the GitLab Selenium Server service
    - mkdir -p selenium/ && curl -s http://localhost:4545/logs.tar.gz | tar -xvzf - -C selenium/
    - mkdir -p selenium/ && curl http://localhost:4545/server-log --output selenium/server-log.txt
  artifacts:
    when: always
    paths:
      - selenium/
  variables:
    SELENIUM_REMOTE_URL: http://localhost:4545/wd/hub
    GITLAB_TARGET_SELENIUM_REMOTE_URL: http://localhost:4444/wd/hub
  tags:
    - docker
    - shared
```


#### Docker service

Point your Selenium tests directly at `http://gitlab-selenium-server:4545/wd/hub`
after including the `gitlab/selenium-server` service.

**Note:** Because the Selenium Server and Proxy are running inside a separate
service container, you won't be able to access `localhost` endpoints from the main app container.
This means that your app needs to be accessible from the internet in order to test this way.
You can [read more about this issue here](https://gitlab.com/gitlab-org/gitlab-selenium-server/issues/1)
and [track GitLab CI support for proper Docker networking in this issue](https://gitlab.com/gitlab-org/gitlab-runner/issues/1042).

`.gitlab-ci.yml`
```yml
test:
  stage: test
  script:
    - npm test
    # Show the logs for the GitLab Selenium Server service
    - curl http://gitlab-selenium-server:4545/logs || true
  artifacts:
    when: always
    paths:
      - selenium/
  variables:
    SELENIUM_REMOTE_URL: http://gitlab-selenium-server:4545/wd/hub
  services:
    - name: registry.gitlab.com/gitlab-org/gitlab-selenium-server:latest
      alias: gitlab-selenium-server
  tags:
    - docker
    - shared
```

### Use your own Selenium server

You can point the proxy at your own Selenium server with the `GITLAB_TARGET_SELENIUM_REMOTE_URL`
environment variable.

`.gitlab-ci.yml`
```yml
test:
  # ...
  variables:
    SELENIUM_REMOTE_URL: http://gitlab-selenium-server:4444/wd/hub
    GITLAB_TARGET_SELENIUM_REMOTE_URL: http://selenium__standalone-chrome:4444/wd/hub
  # ...
```


# Development

```sh
# macOS/Linux
GITLAB_TARGET_SELENIUM_REMOTE_URL=http://localhost:4444/wd/hub npm run start-dev

# Windows
set GITLAB_TARGET_SELENIUM_REMOTE_URL="http://localhost:4444/wd/hub"&&npm run start-dev
```

## CLI parameters

 - `--log-level`: Level of logs in the console (defaults to `info`) (see https://www.npmjs.com/package/winston#logging-levels)
 - `--file-log-level`: Level of logs in the log file (see https://www.npmjs.com/package/winston#logging-levels)
 - `--selenium-proxy-log-dir`: Log directory (can also be passed as a environment variable `GITLAB_SELENIUM_PROXY_LOG_DIR`)
 - `--target-selenium-remote-url`: The endpoint to proxy to (can also be passed as a environment variable `GITLAB_TARGET_SELENIUM_REMOTE_URL`)


### Setting up the GitLab Docker Registry

See https://gitlab.com/snippets/73109#setup-the-container-registry


#### Setup a runner that can run Docker in Docker `dind` builds to push to the registry

Setup a Docker runner, add the privileged mode https://docs.gitlab.com/runner/executors/docker.html#the-privileged-mode
and restart `gitlab-ci-multi-runner restart`


# FAQ

### Use a different Selenium server/hub

Define the `SELENIUM_REMOTE_URL` environment variable. For GitLab CI, we define it in `.gitlab-ci.yml`

ex.
```sh
SELENIUM_REMOTE_URL=http://username:access_token@ondemand.saucelabs.com:80/wd/hub npm test
```

### Start your own Selenium sever

 1. Download your driver of choice
    - For example Chrome, https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in `/usr/local/bin` to get it on your `PATH`
 1. Download Selenium server, http://docs.seleniumhq.org/download/
 1. Start the server
    ```sh
    java -Dwebdriver.chrome.driver=$(which chromedriver) -jar /Users/eric/Downloads/selenium-server-standalone-3.4.0.jar -port 4444
    ```
 1. Access the server with `http://localhost:4545/wd/hub`


# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
