const Promise = require('bluebird');
const path = require('path');
const fs = require('fs-extra');
const emptyDir = Promise.promisify(fs.emptyDir);
const glob = Promise.promisify(require('glob'));
const webdriver = require('selenium-webdriver');
const chromedriver = require('chromedriver');
const request = Promise.promisify(require('request'));

const ChildExecutor = require('../util/child-executor');

// Environment variables
const SELENIUM_REMOTE_URL = process.env['SELENIUM_REMOTE_URL'] || 'http://localhost:4545/wd/hub';
const GITLAB_TARGET_SELENIUM_REMOTE_URL = process.env['GITLAB_TARGET_SELENIUM_REMOTE_URL'] || 'http://localhost:4444/wd/hub';
const GITLAB_SELENIUM_PROXY_LOG_DIR = process.env['GITLAB_SELENIUM_PROXY_LOG_DIR'] ? path.resolve(__dirname, '../../', process.env['GITLAB_SELENIUM_PROXY_LOG_DIR']) : path.resolve(__dirname, '../../test-logs/');

// Test timeouts
const ORIGINAL_TIMEOUT_INTERVAL = jasmine.DEFAULT_TIMEOUT_INTERVAL;
const NEW_TIMEOUT_INTERVAL = 8000;
const REQUEST_TIMEOUT_INTERVAL = 3000;

function waitForServer(url, timeout) {
  let hasTimedOut = false;

  if (timeout >= 0) {
    setTimeout(() => {
      hasTimedOut = true;
    }, timeout);
  }

  return (function waitRecursive() {
      return request({
        uri: url
      })
        .catch((err) => {
          if(!hasTimedOut) {
            return waitRecursive(url);
          } else {
            throw new Error(`Unable to connect to server ${url}, ${err}, ${err.stack}`);
          }
        });
  })();
}

describe('GitLab Selenium Server', () => {
  let gitlabSeleniumServerExecutor;
  let driver;

  beforeEach((done) => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = NEW_TIMEOUT_INTERVAL;

    // Start the target Selenium server
    chromedriver.start([
      '--url-base=wd/hub',
      `--port=4444`,
      '--verbose'
    ]);

    // We clear things out in the beforeEach so if something fails, we can reference the server output
    emptyDir(GITLAB_SELENIUM_PROXY_LOG_DIR)
      // Wait for the target Selenium server to startup
      .then(() => waitForServer('http://localhost:4444/wd/hub/status', REQUEST_TIMEOUT_INTERVAL))
      .then(() => {
        // Start the proxy
        gitlabSeleniumServerExecutor = new ChildExecutor();
        gitlabSeleniumServerExecutor.exec('npm start', {
          env: Object.assign({}, process.env, {
            'GITLAB_SELENIUM_PROXY_LOG_DIR': GITLAB_SELENIUM_PROXY_LOG_DIR,
            'GITLAB_TARGET_SELENIUM_REMOTE_URL': GITLAB_TARGET_SELENIUM_REMOTE_URL
          })
        })
          .catch((resultInfo) => {
            done.fail(resultInfo.error);
          });

        // Wait for our shelled out proxy to startup
        return waitForServer(SELENIUM_REMOTE_URL, REQUEST_TIMEOUT_INTERVAL);
      })
      .then(() => {
        const chromeCapabilities = webdriver.Capabilities.chrome();
        chromeCapabilities.set('chromeOptions', { args: ['--headless'] });

        driver = new webdriver.Builder()
          .forBrowser('chrome')
          .withCapabilities(chromeCapabilities)
          // Automatically pulled from enviornment variable `SELENIUM_REMOTE_URL`
          // This should point at our GitLab Selenium server
          .usingServer(SELENIUM_REMOTE_URL)
          .build();
      })
      .then(done)
      .catch(done.fail);
  });

  afterEach(() => {
    // When we start to use `driver.get` this doesn't let the process exit
    // so we need to use `jest --forceExit`
    if (driver) {
      driver.quit();
    }

    gitlabSeleniumServerExecutor.child.kill();

    chromedriver.stop();

    jasmine.DEFAULT_TIMEOUT_INTERVAL = ORIGINAL_TIMEOUT_INTERVAL;
  });

  it('Takes screenshot on key stroke', (done) => {
    driver.get('http://google.com/')
      .then(() => {
        const searchInput = driver.findElement(webdriver.By.name('q'))
        searchInput.sendKeys('webdriver')
        searchInput.sendKeys(webdriver.Key.ENTER);

        return driver.wait(webdriver.until.titleIs('webdriver - Google Search'), 1000);
      })
      .then(() => driver.quit())
      // Assert screenshots
      .then(() => {
        return glob(path.join(GITLAB_SELENIUM_PROXY_LOG_DIR, './*/screenshots/*.png'))
          .then((files) => expect(files.length).toBe(3));
      })
      // Assert JSON log file
      .then(() => {
        return glob(path.join(GITLAB_SELENIUM_PROXY_LOG_DIR, './*/selenium-logs.json'))
          .then((files) => expect(files.length).toBe(1));
      })
      .then(done)
      .catch(done.fail);
  });
});
